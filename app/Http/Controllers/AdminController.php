<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function admin()
    {
        return view('admin/dashboard');
    }

    public function pages()
    {
        return view('admin/pages');
    }
    public function post()
    {
        return view('admin/post');
    }
    public function comment()
    {
        return view('admin/comment');
    }
    public function email()
    {
        return view('admin/email');
    }
    public function users()
    {
        return view('admin/users');
    }
}
