<li class="header">MAIN NAVIGATION</li>
<li>
    <a href="/admin">
    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
</li>
<li>
    <a href="/admin/pages">
        <i class="fa fa-pagelines"></i> <span>Pages</span>
    </a>
</li>
<li>
    <a href="/admin/post">
        <i class="fa fa-database"></i> <span>Post</span>
    </a>
</li>
<li>
    <a href="/admin/comment">
        <i class="fa fa-comment"></i> <span>Comment</span>
    </a>
</li>
<li>
    <a href="/admin/email">
    <i class="fa fa-envelope"></i> <span>E-mail</span></a>
</li>
<li>
    <a href="/admin/user">
    <i class="fa fa-user"></i> <span>Users</span></a>
</li>
